# Target usage

```html
    <div>
        <label for="age">Age</label>
        <input name="age" id="age" type="number" min="0" max="99" />
    </div>
```

```js
    $(function(){
        $("#age").scrollerInput({
            height: 100
        });
    })
```